import React, {Component} from 'react'
import {View, Text} from 'react-native'
import AddTodo from "./containers/AddTodo";
import VisibleTodo from './containers/VisibleTodo'


class TodoApp extends Component {
    render() {
        return (
            <View style={styles.containerStyle}>
                <AddTodo/>
                <View>
                    <VisibleTodo/>
                </View>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        marginTop: 40,
    }
}

export default TodoApp