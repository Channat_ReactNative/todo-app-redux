import React, {Component} from 'react'
import {View, Text, TextInput, Button} from 'react-native'
import {connect} from 'react-redux'
import {addTodo} from "../actions";

class AddTodo extends Component {

    state = {
        text: ''
    }

    addTodo = (text) => {
        // update store
        this.props.dispatch(addTodo(text))
        this.setState({text: ''})
        // console.log("===> ", this.state.text)
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <TextInput  value={this.state.text} onChangeText={(text) => this.setState({text})} style={styles.textInputStyle} placeholder={'Add Todo'}/>
                <Button onPress={() => this.addTodo(this.state.text)} style={styles.buttonStyle} title={"+"}/>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',

    },
    textInputStyle: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#ddd',
        marginLeft: 5,
        marginRight: 5,
        paddingLeft: 5,
        paddingRight: 5,
        height: 50,
        flex: 1
    },
    buttonStyle: {
        fontWeight: 500,
        size: 50
    }
}


export default connect() (AddTodo)