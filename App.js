/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import TodoApp from "./src/TodoApp";
import {Provider} from 'react-redux'
import {store} from "./src/store";

export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <TodoApp/>
            </Provider>
        );
    }
}
